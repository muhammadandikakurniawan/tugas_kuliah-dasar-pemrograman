import os
from ..objects import *
import re
from ..repositories import *
from ..utilities.EmailUtil import *
from ..utilities.AppSettings import *
from asgiref.sync import sync_to_async
from multiprocessing import Process
import codecs
from ..entities import *
import hashlib
from datetime import datetime
from datetime import timedelta
import uuid
from django.http import HttpResponse, response  
from django.core.files.storage import FileSystemStorage
from django.core.files.storage import default_storage
import time
from PIL import Image
from multiprocessing import Process

class ProductService():

    @staticmethod
    def Add(userId, name, description, images, stock, price):
        
        result = ServiceResultObject()

        try:
            print("============================= add product =================================")

            productId = str(uuid.uuid4())

            imgCount = 0

            listImage = []
   
            for key, img in images.items():

                imgExt = img.name.split(".")[1]

                newFileName = "PRD-{0}-{1}-{2}".format(
                    userId,
                    str(time.time()).replace('.',''),
                    str(imgCount)
                )

                newFileName = newFileName+"."+imgExt

                dstFileName = AppSettings.ProductImagesPath+newFileName

                dst = open(dstFileName ,'wb')
                
                dst.write(img.read())

                dst.close()

                currImg = Image.open(dstFileName)
                new_image = currImg.resize((400, 400))
                new_image.save(dstFileName)
                imgCount += 1

                listImage.append(newFileName)
            
            dataInsertProduct = ProductEntity()

            dataInsertProduct.UserId = userId
            dataInsertProduct.Name = name
            dataInsertProduct.Description = description
            dataInsertProduct.Images = listImage
            dataInsertProduct.Stock = stock
            dataInsertProduct.Price = price

            insertResult = ProductRepo.Insert(dataInsertProduct)
            
            if(insertResult.IsSuccess == False):
                result.Message = insertResult.Message
                result.StatusCode = "13"
                return result

            result.Data = insertResult.Value.Id

        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result

    @staticmethod
    def GetAll():
        
        result = {
            "Data" : [],
            "StatusCode" : "00",
            "Message" : ""
        }

        try:
            getDataProduct = ProductRepo.GetAll()
            
            if getDataProduct.IsSuccess == False:
                result.Message = getDataProduct.Message
                result.StatusCode = "10"
                return result
            
            datas = []

            for d in getDataProduct.Value:
                datas.append(d.__dict__)

            print("============= get all product =============")
            print(getDataProduct.__dict__)

            result["Data"] = datas

        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result

    @staticmethod
    def GetByUserId(userId):
        
        result = {
            "Data" : [],
            "StatusCode" : "00",
            "Message" : ""
        }

        try:
            print("============= get ByUserId =============")
            print(userId)
            # if (re.search("^\w+$", str(userId))) == None :
            #     result.Message = "user id cannot be empty"
            #     result.StatusCode = "12"
            #     return result

            print("============= retrive data =============")

            getDataProduct = ProductRepo.GetByUserId(str(userId))

            print(getDataProduct.__dict__)
            
            if getDataProduct.IsSuccess == False:
                result.Message = getDataProduct.Message
                result.StatusCode = "10"
                return result
            
            datas = []

            for d in getDataProduct.Value:
                datas.append(d.__dict__)

            result["Data"] = datas

        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result

    @staticmethod
    def GetById(id):
        
        result = {
            "Data" : [],
            "StatusCode" : "00",
            "Message" : ""
        }

        try:
            print("============= get ById =============")
            print(id)
            # if (re.search("^\w+$", str(userId))) == None :
            #     result.Message = "user id cannot be empty"
            #     result.StatusCode = "12"
            #     return result

            print("============= retrive data =============")

            getDataProduct = ProductRepo.GetById(id)

            print(getDataProduct.__dict__)
            
            if getDataProduct.IsSuccess == False:
                result.Message = getDataProduct.Message
                result.StatusCode = "10"
                return result

            result["Data"] = getDataProduct.Value.__dict__ if getDataProduct.Value is not None else None

        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result
    
    @staticmethod
    def Delete(id):
        
        result = ServiceResultObject()

        try:
            print("============= delete =============")
            print(id)

            print("============= retrive data =============")

            query = {"Id":id}
            newValue = {"$set":{"IsDeleted":True}}
            updateRes = ProductRepo.Update(query,newValue)

            if updateRes.IsSuccess == False:
                result.Message = updateRes.Message
                result.StatusCode = "10"
                return result

        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result

    @staticmethod
    def EditStock(productId, stock):
        
        result = ServiceResultObject()

        try:
            print("============= edit stock =============")
            print(productId)
            print(stock)

            print("============= retrive data =============")

            query = {"Id":productId}
            newValue = {"$set":{"Stock":int(stock)}}
            updateRes = ProductRepo.Update(query,newValue)

            if updateRes.IsSuccess == False:
                result.Message = updateRes.Message
                result.StatusCode = "10"
                return result

        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result

    @staticmethod
    def AddOrder(userId, productId, qty, address, msg):
        
        result = ServiceResultObject()

        try:
            print("============================= add Order =================================")


            print(userId)
            print(productId)
            print(qty)
            print(address)
            print(msg)
            
            print("============================= get data product =================================")
            dataProduk = ProductRepo.GetById(productId).Value
            print(dataProduk.__dict__)

            print("============================= get data user =================================")
            dataUser = UserRepo.GetById(userId).Value
            print(dataUser.__dict__)

            print("============================= check dataProduk is None =================================")
            if(dataProduk is None):
                result.Message = "product is not found"
                result.StatusCode = "10"
                return result

            print("============================= check dataProduk.Stock =================================")
            if(dataProduk.Stock <= 0):
                result.Message = "stock is empty"
                result.StatusCode = "11"
                return result
            
            print("============================= check dataProduk.Stock > qty =================================")
            if(dataProduk.Stock < qty):
                result.Message = "quantity cannot more than product's stocks"
                result.StatusCode = "12"
                return result
            
            print("============================= get data Owner =================================")
            dataOwner = UserRepo.GetById(dataProduk.UserId).Value
            print(dataOwner.__dict__)

            #edit stock
            print("============================= edit stock =================================")
            newStock = dataProduk.Stock - qty
            print(newStock)

            print("============================= updateStock =================================")
            query = {"Id":productId}
            newValue = {"$set":{"Stock":int(newStock)}}
            editStockRes = ProductRepo.Update(query,newValue)
            print(editStockRes.IsSuccess)
            if(editStockRes.IsSuccess == False):
                result.Message = "Edit stock failed : "+editStockRes.Message
                result.StatusCode = "13"
                return result
            
            #insert data order
            dataInsertOrder = OrderEntity()
            dataInsertOrder.UserId = userId
            dataInsertOrder.Quantity = qty
            dataInsertOrder.ProductId = productId
            dataInsertOrder.Address = address

            insertOrderRes = OrderRepo.Insert(dataInsertOrder)
            print("============================= insert order =================================")
            print(insertOrderRes.IsSuccess )
            if(insertOrderRes.IsSuccess == False):
                result.Message = "Insert data order failed : "+dataInsertOrder.Message
                result.StatusCode = "14"
                return result
            
            result.Data = insertOrderRes.Value.Id
            result.StatusCode = "00"

            #send email to owner
            msg = str(msg).replace("\n","<br/>")
            Process(target=SendEmailOrder,args=(
                dataOwner.Email, 
                productId, 
                dataProduk.Name, 
                dataUser.Email,
                qty,
                address,
                msg
                )
            ).start()

        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result


    @staticmethod
    def SendEmailProductMessage(senderId, productId, msg):
        
        result = ServiceResultObject()

        try:
            print("============= send email SendEmailProductMessage =============")
            print(senderId)
            print(productId)
            
            print("================================ data params =============================")

            dataSender = UserRepo.GetById(senderId).Value
            dataProduct = ProductRepo.GetById(productId).Value
            dataOwner = UserRepo.GetById(dataProduct.UserId).Value

            print("================== data sender =====================")
            print(dataSender.__dict__)
            print("================== data product =====================")
            print(dataProduct.__dict__)
            print("================== data owner =====================")
            print(dataOwner.__dict__)

            #send email process
            print("================================= MESSAGE =====================================")
            print(msg)
            
            print("================================= MESSAGE EDITED =====================================")
            msg = str(msg).replace("\n","<br/>")
            print(msg)

            #send email
            Process(target=SendEmailMsgProduct,args=(
                dataSender.Email, 
                dataOwner.Email, 
                dataOwner.Username, 
                msg,
                dataProduct.Name)
            ).start()


        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result