from ..objects import *
import re
from ..repositories import *
from ..utilities.EmailUtil import *
from asgiref.sync import sync_to_async
from multiprocessing import Process
import codecs
from ..entities import *
import hashlib
from datetime import datetime
from datetime import timedelta
import uuid
from django.http import HttpResponse, response  
class AuthService():

    @staticmethod
    def Register(params):
        
        result = ServiceResultObject()

        try:
            
            dataExist = UserRepo.GetByEmail(params.Email)
            if dataExist.Value is not None:
                result.Message = "email already used"
                result.StatusCode = "10"
                return result
            
            if (re.search("^\w+$", params.Username)) == None :
                result.Message = "username cannot empty"
                result.StatusCode = "12"
                return result
            
            if(params.Password != params.ConfirmPassword):
                result.Message = "password not valid"
                result.StatusCode = "11"
                return result

            dataInsert = UserEntity()
            dataInsert.Email = params.Email
            dataInsert.Username = params.Username

            hash_object = hashlib.sha256(str(params.Password).encode('utf-8'))
            passwordHash = hash_object.hexdigest()

            dataInsert.Password = passwordHash

            insertUserRes =  UserRepo.Insert(dataInsert)

            if(insertUserRes.IsSuccess == False):
                result.Message = insertUserRes.Message
                result.StatusCode = "13"
                return result

            dataInsertToken = UserRegisterTokenEntity()
            dataInsertToken.UserId = insertUserRes.Value.Id

            setTokenPlain = str(insertUserRes.Value.Id)+str(insertUserRes.Value.Email)+str(insertUserRes.Value.Username)+str(insertUserRes.Value.Password)

            tokenHashObj = hashlib.sha256(str(setTokenPlain).encode('utf-8'))
            token = tokenHashObj.hexdigest()

            dataInsertToken.Token = token
            dataInsertToken.ExpirationDate = datetime.now() + timedelta(seconds=1800)
            UserRegisterTokenRepo.Insert(dataInsertToken)

            #send email
            Process(target=SendEmailRegister,args=(params.Email,params.Username,token,)).start()
            
        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result
    
    @staticmethod
    def VerifyAccount(email,token):
        result = ServiceResultObject()
        try:
            dataUser = UserRepo.GetByEmail(email)
            if dataUser.Value is None:
                result.Message = "user is nnot valid"
                result.StatusCode = "10"
                return result
            
            if dataUser.Value.IsActive:
                result.Message = "cannot activate again"
                result.StatusCode = "13"
                return result

            dataToken = UserRegisterTokenRepo.GetByUserIdToken(dataUser.Value.Id,token)
            if dataToken.Value is None:
                result.Message = "token is nnot valid"
                result.StatusCode = "11"
                return result


            #update data user to set active
            query = {"Id":dataUser.Value.Id}
            newValue = {"$set":{"IsActive":True}}
            updateRes = UserRepo.Update(query,newValue)
            if(updateRes.IsSuccess == False):
                result.Message = "update data user failed : "
                result.StatusCode = "12"
                return result

            result.StatusCode = "00"
            result.Data = dataToken.Value.__dict__
        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"
        
        return result

    @staticmethod
    def Login(params):
        
        result = ServiceResultObject()

        try:
            
            dataUserByEmail = UserRepo.GetByEmail(params.Email)
            if dataUserByEmail.Value == None:
                result.Message = "email not valid"
                result.StatusCode = "10"
                return result
            
            hash_object = hashlib.sha256(str(params.Password).encode('utf-8'))
            passwordHash = hash_object.hexdigest()
            if dataUserByEmail.Value.Password != passwordHash:
                result.Message = "password wrong"
                result.StatusCode = "11"
                return result
            
            if dataUserByEmail.Value.IsActive == False:
                result.Message = "user is not active"
                result.StatusCode = "11"
                return result
            
            #insert token
            dataInsertAccessToken = UserAccessToken()
            token = str(str(uuid.uuid4()) + str(uuid.uuid4()) + str(uuid.uuid4()) + str(uuid.uuid4()))
            token = token.replace('-','')
            dataInsertAccessToken.Token = token
            dataInsertAccessToken.UserId = dataUserByEmail.Value.Id
            
            createTokenRes = UserAccessTokenRepo.Insert(dataInsertAccessToken)
            if(createTokenRes.IsSuccess == False):
                result.Message = "create token error"
                result.StatusCode = "13"
                return result
            
            result.Data = {
                "Token" : dataInsertAccessToken.Token,
                "UserId" : dataInsertAccessToken.UserId
            }
        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result

    @staticmethod
    def GetUserByAccessToken(params):
        
        result = ServiceResultObject()

        try:
            print("============================== GetUserByAccessToken ==============================")
            print(params)
            print("============================== GetUserByAccessToken ==============================")
            dataToken = UserAccessTokenRepo.GetByToken(params)
            if dataToken.Value == None:
                result.Message = "token not valid"
                result.StatusCode = "10"
                return result
            
            dataUser = UserRepo.GetById(dataToken.Value.UserId)

            if dataUser.Value == None:
                result.Message = "User not found"
                result.StatusCode = "11"
                return result
            
            result.Data = dataUser.Value.__dict__
            
        except Exception as ex:
            result.Message = str(ex)
            result.StatusCode = "500"

        return result