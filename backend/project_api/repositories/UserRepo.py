from ..dbcontext.ProjectDb import ProjectDb
from ..objects import *
from ..entities.UserEntity import UserEntity
import datetime
import uuid
class UserRepo:

    @staticmethod
    def Insert(param):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user"]

            param.IsActive = False
            param.IsDeleted = False
            param.CreatedDate = datetime.datetime.now()
            param.Id = str(uuid.uuid4())

            resInsert = table.insert_one(param.__dict__)
            res.Value = param
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res

    @staticmethod
    def Update(query,newValue):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user"]

            updateRes = table.update_many(query,newValue)
            res.Value = table.find(query)
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res
    
    @staticmethod
    def GetAll():

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user"]
            datas = table.find()
            res.IsSuccess = True
            res.Value = []

            for d in datas:
                model = UserEntity()
                model.Email = d["Email"]
                model.Username = d["Username"]
                model.Password = d["Password"]
                model.CreatedDate = d["CreatedDate"]
                model.IsDeleted = d["IsDeleted"]
                model.IsActive = d["IsActive"]
                model.Id = d["Id"]
                res.Value.append(model)

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res


    @staticmethod
    def GetById(userId):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user"]
            datas = table.find({"Id":str(userId)})
            res.IsSuccess = True

            for d in datas:

                if(res.Value is None):
                    model = UserEntity()
                    model.Id = d["Id"]
                    model.Email = d["Email"]
                    model.Username = d["Username"]
                    model.Password = d["Password"]
                    model.CreatedDate = d["CreatedDate"]
                    model.IsDeleted = d["IsDeleted"]
                    model.IsActive = d["IsActive"]
                    res.Value = model

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res

    @staticmethod
    def GetByEmail(param):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user"]
            datas = table.find({"Email":str(param)})
            res.IsSuccess = True

            for d in datas:

                if(res.Value is None):
                    model = UserEntity()
                    model.Id = d["Id"]
                    model.Email = d["Email"]
                    model.Username = d["Username"]
                    model.Password = d["Password"]
                    model.CreatedDate = d["CreatedDate"]
                    model.IsDeleted = d["IsDeleted"]
                    model.IsActive = d["IsActive"]
                    res.Value = model

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res