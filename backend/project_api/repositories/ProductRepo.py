from ..dbcontext.ProjectDb import ProjectDb
from ..objects import *
from ..entities.ProductEntity import ProductEntity
import datetime
import uuid

class ProductRepo:

    @staticmethod
    def Insert(param):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["product"]

            param.IsDeleted = False
            param.CreatedDate = datetime.datetime.now()
            param.Id = str(uuid.uuid4())

            resInsert = table.insert_one(param.__dict__)
            res.Value = param
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res

    @staticmethod
    def Update(query,newValue):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["product"]

            updateRes = table.update_many(query,newValue)
            res.Value = table.find(query)
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res
    

    @staticmethod
    def GetAll():

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["product"]
            datas = table.find({"IsDeleted":False})
            data = [] if datas is None else datas
            res.IsSuccess = True
            res.Value = []

            for d in datas:
                model = ProductEntity()
                model.CreatedDate = d["CreatedDate"]
                model.IsDeleted = d["IsDeleted"]
                model.Id = d["Id"]
                model.UserId = d["UserId"]
                model.Name = d["Name"]
                model.Description = d["Description"]
                model.Images = d["Images"]
                model.Stock = d["Stock"]
                model.Price = d["Price"]

                res.Value.append(model)

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res


    @staticmethod
    def GetById(id):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["product"]
            datas = table.find({"Id":id})
            data = [] if datas is None else datas
            res.Value = None
            res.IsSuccess = True

            for d in datas:

                if(res.Value is None):
                    model = ProductEntity()
                    model.CreatedDate = d["CreatedDate"]
                    model.IsDeleted = d["IsDeleted"]
                    model.Id = d["Id"]
                    model.UserId = d["UserId"]
                    model.Name = d["Name"]
                    model.Description = d["Description"]
                    model.Images = d["Images"]
                    model.Stock = d["Stock"]
                    model.Price = d["Price"]
                    res.Value = model

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res


    @staticmethod
    def GetByUserId(param):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["product"]
            datas = table.find({"$and":[{"UserId":str(param)},{"IsDeleted":False}]})
            data = [] if datas is None else datas
            res.Value = []
            res.IsSuccess = True
            
            for d in datas:
                model = ProductEntity()
                model.CreatedDate = d["CreatedDate"]
                model.IsDeleted = d["IsDeleted"]
                model.Id = d["Id"]
                model.UserId = d["UserId"]
                model.Name = d["Name"]
                model.Description = d["Description"]
                model.Images = d["Images"]
                model.Stock = d["Stock"]
                model.Price = d["Price"]
                res.Value.append(model)

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res