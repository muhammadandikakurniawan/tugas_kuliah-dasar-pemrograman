from ..dbcontext.ProjectDb import ProjectDb
from ..objects import *
from ..entities.ProductEntity import ProductEntity
import datetime
import uuid

class OrderRepo:

    @staticmethod
    def Insert(param):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["order"]

            param.IsDeleted = False
            param.CreatedDate = datetime.datetime.now()
            param.Id = str(uuid.uuid4())

            resInsert = table.insert_one(param.__dict__)
            res.Value = param
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res

    @staticmethod
    def Update(query,newValue):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["order"]

            updateRes = table.update_many(query,newValue)
            res.Value = table.find(query)
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res
    
