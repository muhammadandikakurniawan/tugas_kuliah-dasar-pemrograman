from ..dbcontext.ProjectDb import ProjectDb
from ..objects import *
from ..entities.UserAccessToken import UserAccessToken
import datetime
import uuid
class UserAccessTokenRepo:

    @staticmethod
    def Insert(param):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user_access_token"]

            param.CreatedDate = datetime.datetime.now()
            param.Id = str(uuid.uuid4())
            param.IsDeleted = False
            resInsert = table.insert_one(param.__dict__)
            res.Value = param
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res
    
    @staticmethod
    def GetAll():

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user_access_token"]
            datas = table.find()
            res.IsSuccess = True
            res.Value = []

            for d in datas:
                model = UserAccessToken()
                model.Id = d["Id"]
                model.UserId = d["UserId"]
                model.Token = d["Token"]
                model.CreatedDate = d["CreatedDate"]
                model.IsDeleted = d["IsDeleted"]
                res.Value.append(model)

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res

    @staticmethod
    def GetByToken(token):

        res = RepoResultObject()

        try:

            table = ProjectDb.GetDb()["user_access_token"]
            datas = table.find({"Token":str(token)})
            res.IsSuccess = True

            for d in datas:
                print(d)
                if(res.Value is None):
                    model = UserAccessToken()
                    model.Id = d["Id"]
                    model.UserId = d["UserId"]
                    model.Token = d["Token"]
                    model.CreatedDate = d["CreatedDate"]
                    model.IsDeleted = d["IsDeleted"]
                    
                    res.Value = model

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res

    @staticmethod
    def GetByUserId(userID):

        res = RepoResultObject()

        try:

            table = ProjectDb.GetDb()["user_access_token"]
            datas = table.find({"UserId":str(userID)})
            res.IsSuccess = True

            for d in datas:
                print(d)
                if(res.Value is None):
                    model = UserAccessToken()
                    model.Id = d["Id"]
                    model.UserId = d["UserId"]
                    model.Token = d["Token"]
                    model.CreatedDate = d["CreatedDate"]
                    model.IsDeleted = d["IsDeleted"]
                    
                    res.Value = model

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res