from ..dbcontext.ProjectDb import ProjectDb
from ..objects import *
from ..entities.UserRegisterTokenEntity import UserRegisterTokenEntity
import datetime
import uuid
class UserRegisterTokenRepo:

    @staticmethod
    def Insert(param):

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user_register_token"]

            param.CreatedDate = datetime.datetime.now()
            param.Id = str(uuid.uuid4())

            resInsert = table.insert_one(param.__dict__)
            res.Value = param
            res.IsSuccess = True
            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res
    
    @staticmethod
    def GetAll():

        res = RepoResultObject()

        try:
            
            table = ProjectDb.GetDb()["user_register_token"]
            datas = table.find()
            res.IsSuccess = True
            res.Value = []

            for d in datas:
                model = UserRegisterTokenEntity()
                model.Id = d["Id"]
                model.UserId = d["UserId"]
                model.Token = d["Token"]
                model.CreatedDate = d["CreatedDate"]
                model.ExpirationDate = d["ExpirationDate"]
                res.Value.append(model)

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res

    @staticmethod
    def GetByUserIdToken(userID, token):

        res = RepoResultObject()

        try:

            table = ProjectDb.GetDb()["user_register_token"]
            datas = table.find({"UserId":str(userID),"Token":str(token)})
            res.IsSuccess = True

            for d in datas:
                print(d)
                if(res.Value is None):
                    model = UserRegisterTokenEntity()
                    model.Id = d["Id"]
                    model.UserId = d["UserId"]
                    model.Token = d["Token"]
                    model.CreatedDate = d["CreatedDate"]
                    model.ExpirationDate = d["ExpirationDate"]
                    
                    res.Value = model

            return res
        except Exception as ex:
            res.Message = str(ex)
            res.IsSuccess = False
        
        return res