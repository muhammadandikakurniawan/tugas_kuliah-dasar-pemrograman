import smtplib
from smtplib import SMTPException
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from asgiref.sync import async_to_sync

def sendEmail(receiver, mail_content):
    try:
        #The mail addresses and password
        sender_address = 'project.local000@gmail.com'
        sender_pass = '06234shbtslm'

        #Setup the MIME
        message = MIMEMultipart()
        message['From'] = sender_address
        message['To'] = receiver
        message['Subject'] = 'project kelompok3'   #The subject line

        #The body and the attachments for the mail
        message.attach(MIMEText(mail_content, 'html'))

        #Create SMTP session for sending the mail
        session = smtplib.SMTP('smtp.gmail.com', 587) #use gmail with port
        session.starttls() #enable security
        session.login(sender_address, sender_pass) #login with mail_id and password
        text = message.as_string()
        session.sendmail(sender_address, receiver, text)
        session.quit()
        print('Mail Sent')   
        return True
    except Exception as ex:
        print("============================= send email error ==================================")
        print(str(ex))
        return False  

def SendEmailRegister(receiver, username, token):
    print("================ send email ===============================")

    try:
        mail_content = open("project_api/app_data/RegisterationEmailTemplate.html", "r").read()
        mail_content = mail_content.replace("{user_name}",username)
        linkVerify = "http://127.0.0.1:8000/AuthService/VerifyAccount/{0}/{1}".format(receiver,token)
        mail_content = mail_content.replace("{link}","http://localhost:3000/VerifyAccount/{0}/{1}".format(receiver,token))
        
        return sendEmail(receiver,mail_content)
    except Exception as ex:
        print("============================= send email error ==================================")
        print(str(ex))
        return False

def SendEmailMsgProduct(senderMsg, receiver, username, msg, productName):
    print("================ send email SendEmailMsgProduct ===============================")

    try:
        mail_content = open("project_api/app_data/MessageProductOwnerEmailTemplate.html", "r").read()
        mail_content = mail_content.replace("{username}",username)
        mail_content = mail_content.replace("{senderMsg}",senderMsg)
        mail_content = mail_content.replace("{product_name}",productName)
        mail_content = mail_content.replace("{msg}",msg)
        
        return sendEmail(receiver,mail_content)
    except Exception as ex:
        print("============================= send email error ==================================")
        print(str(ex))
        return False

def SendEmailOrder(receiver, productId, productName, orderOwner, qty, address, msg):
    print("================ send email SendEmailOrder ===============================")
    print(receiver)
    print(productId)
    print(productName)
    print(orderOwner)
    print(qty)
    print(address)
    print(msg)
    try:
        mail_content = open("project_api/app_data/OrderEmailTemplate.html", "r").read()
        mail_content = mail_content.replace("{productId}",productId)
        mail_content = mail_content.replace("{productName}",productName)
        mail_content = mail_content.replace("{orderOwner}",orderOwner)
        mail_content = mail_content.replace("{qty}",str(qty))
        mail_content = mail_content.replace("{address}",address)
        mail_content = mail_content.replace("{msg}",msg)
        
        return sendEmail(receiver,mail_content)
    except Exception as ex:
        print("============================= send email error ==================================")
        print(str(ex))
        return False