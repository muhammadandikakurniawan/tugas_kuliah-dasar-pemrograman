from project_api.objects.ServiceResultObject import ServiceResultObject
from rest_framework.response import Response
import json
from django.http import HttpResponseForbidden   
class AuthMiddleware:

    get_response = None
    def __init__(self,get_response):
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<< init middleware >>>>>>>>>>>>>>>>>>>>>>>>>")
        self.get_response = get_response
    
    def __call__(self,request):
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<< call every request middleware >>>>>>>>>>>>>>>>>>>>>>>>>")
        
        # if(("Authorization" in request.headers.keys()) == False):
        #     errorRes = ServiceResultObject()
        #     errorRes.StatusCode = "403"
        #     errorRes.Message = "cannot access this service"
        #     response = HttpResponseForbidden()
        #     response.content = json.dumps(errorRes.__dict__)
        #     return response
        
        # if((request.headers["Authorization"] == "Barier 123asd456qwe789hjk") == False):
        #     errorRes = ServiceResultObject()
        #     errorRes.StatusCode = "403"
        #     errorRes.Message = "cannot access this service"
        #     response = HttpResponseForbidden()
        #     response.content = json.dumps(errorRes.__dict__)
        #     return response

        response = self.get_response(request)
        return response
    
    def process_view(self,request,view_func,*view_args,**view_kargs):
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<< call just before execute controller >>>>>>>>>>>>>>>>>>>>>>>>>")
        # print(request.__dict__)