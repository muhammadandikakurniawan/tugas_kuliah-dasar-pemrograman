from rest_framework import routers
from .controllers.AuthController import AuthController
from .controllers.ProductController import ProductController

router = routers.DefaultRouter()

router.register("AuthService",AuthController, basename="AuthService")
router.register("ProductService",ProductController, basename="ProductService")


urlpatterns = router.urls
