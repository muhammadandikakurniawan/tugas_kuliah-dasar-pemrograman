import sys
sys.path.append("..")
from collections import namedtuple
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import action
from ..objects import *
from ..services import *
from ..repositories import *
from django.core.files.storage import FileSystemStorage
from django.core import serializers
from django.http import HttpResponse
import json

class ProductController(viewsets.ViewSet):

    @action(methods=["post"], detail=False)
    def Add(self, request):
        result = ServiceResultObject()
        try:
            files = request.FILES
            name = request.data["Name"]
            description = request.data["Description"]
            userId = request.data["UserId"]
            stock = request.data["Stock"]
            price = request.data["Price"]

            result = ProductService.Add(userId, name, description, files, int(stock), int(price))

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result.__dict__)


    @action(methods=["post"], detail=False)
    def EditStock(self, request):
        result = ServiceResultObject()
        try:
            id = request.data["Id"]
            stock = request.data["Stock"]

            result = ProductService.EditStock(id, int(stock))

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result.__dict__)


    @action(methods=["post"], detail=False)
    def GetAll(self, request):
        result = ServiceResultObject()
        try:
           
           result = ProductService.GetAll()

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result)


    @action(methods=["post"], detail=False)
    def GetByUserId(self, request):
        result = ServiceResultObject()
        try:
           
           result = ProductService.GetByUserId(request.data["UserId"])

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result)


    @action(methods=["post"], detail=False)
    def GetById(self, request):
        result = ServiceResultObject()
        try:
           
           result = ProductService.GetById(request.data["Id"])

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result)


    @action(methods=["post"], detail=False)
    def AddOrder(self, request):
        result = ServiceResultObject()
        try:
           
           userId = request.data["UserId"]
           productId = request.data["ProductId"]
           qty = int(request.data["Quantity"])
           msg = request.data["Message"]
           address = request.data["Address"]

           result = ProductService.AddOrder(userId, productId, qty, address, msg)

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result.__dict__)


    @action(methods=["post"], detail=False)
    def Delete(self, request):
        result = ServiceResultObject()
        try:
           
           result = ProductService.Delete(request.data["Id"])

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result.__dict__)


    @action(methods=["post"], detail=False)
    def SendEmailProductMessage(self, request):
        result = ServiceResultObject()
        try:
           
           result = ProductService.SendEmailProductMessage(
               request.data["SenderId"],
               request.data["ProductId"],
               request.data["Msg"]
            )

        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result.__dict__)

