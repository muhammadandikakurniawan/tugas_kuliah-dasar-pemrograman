import sys
sys.path.append("..")
from collections import namedtuple
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import action
from ..objects import *
from ..services import *
from ..repositories import *

class AuthController(viewsets.ViewSet):

    @action(methods=["post"], detail=False)
    def Register(self, request):
        result = ServiceResultObject()
        try:
            print(request.data)
            requestMapped = namedtuple('RegisterParams', request.data.keys())(*request.data.values())
            result = AuthService.Register(requestMapped)
           
        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result.__dict__)

    # @action(methods=["post"], detail=False,url_path="VerifyAccount/(?P<email>.+)/(?P<token>.+)")
    # def VerifyAccount(self, request,email,token):
    @action(methods=["post"], detail=False)
    def VerifyAccount(self, request):
        result = ServiceResultObject()

        try:
            print("=================================== verify account ==================================")

            result = AuthService.VerifyAccount(request.data["Email"],request.data["Token"])
           
        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  Response(result.__dict__)


    @action(methods=["post"], detail=False)
    def Login(self, request):
        result = ServiceResultObject()

        try:
            requestMapped = namedtuple('LoginParams', request.data.keys())(*request.data.values())
            result = AuthService.Login(requestMapped)
            result = Response(result.__dict__)
            print(result.data["StatusCode"])
            if(result.data["StatusCode"] == "00"):
                result.set_cookie("Access_Token_Project",result.data["Data"]["Token"])
        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return  result

    # @action(methods=["post"], detail=False, url_path="GetUserByAccessToken/(?P<token>.+)")
    # def GetUserByAccessToken(self, request, token):
    @action(methods=["post"], detail=False)
    def GetUserByAccessToken(self, request):
        result = ServiceResultObject()

        try:
            requestMapped = namedtuple('Params', request.data.keys())(*request.data.values())
            result = AuthService.GetUserByAccessToken(requestMapped.Token)
        except Exception as ex:
            result.StatusCode = "500"
            result.Message = str(ex)

        return Response(result.__dict__)